def chop(t):
    del t[0]
    del t[len(t) - 1]

def middle(t):
    return t[1:len(t) - 1]

'''
test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(chop(test))
print(test)
print(middle(test))
print(test)
'''
