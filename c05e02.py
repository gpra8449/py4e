minimum = None
maximum = None

while True:
    line = input("Enter a number: ")

    if line == "done":
        break

    try:
        number = float(line)
    except:
        print("Invalid input")
        continue

    if (minimum == None and maximum == None):
        maximum = number
        minimum = number

    if maximum < number:
        maximum = number

    if minimum > number:
        minimum = number

print(maximum, minimum)
