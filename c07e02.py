fname = input("Enter a file name: ")

try:
    fhand = open(fname)
except:
    print("File cannot be opened: ", fname)
    exit()

count = 0
total = 0
average = 0

for line in fhand:
    line = line.rstrip()
    if not line.startswith("X-DSPAM-Confidence:"):
        continue
    count += 1
    total += float(line[line.find(": ") + 1:])

if count > 0:
    average = total / count

print("Average spam confidence: ", average)
