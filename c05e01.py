total = 0
count = 0
average = 0

while True:
    line = input("Enter a number: ")

    if line == 'done':
        break

    try:
        number = float(line)
        total += number
        count += 1
    except:
        print("Invalid input")
        continue

if count > 0:
    average = total / count

print(total, count, average)
