def computepay(hours, rate):
    if hours > 40:
        pay = 40 * rate + (hours - 40) * rate * 1.5
    else:
        pay = hours * rate
    return pay

try:
    input_hours = float(input("Enter Hours: "))
    input_rate = float(input("Enter Rate: "))
except:
    print("Error, please enter numeric input")
    exit()

print("Pay: ", computepay(input_hours, input_rate))
